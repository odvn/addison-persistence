package com.addison.demo.test;
 
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
 

import com.addison.persistence.entity.UserSystemEntity;
import com.addison.persistence.dao.UserSystemDAO;

/**
 * UserSystemDAOTest class.
 * @author Oscar del Valle Narvaez
 */
@ContextConfiguration(locations = "classpath:application-context-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserSystemDAOTest {
    /**
     * DAO to be tested.
     */
    @Autowired
    private UserSystemDAO userSystemDAO;

    /**
     * Test the adding of a new user in the database.
     */
    @Test
    @Transactional
    @Rollback(true)
    public void testAddUser() {
        UserSystemEntity user = new UserSystemEntity("user1", "pass");
        Assert.assertEquals(true, userSystemDAO.addUser(user));
    }

    /**
     * Test the removal of a user in the database.
     */
    @Test
    @Transactional
    @Rollback(true)
    public void testRemoveUser() {
        UserSystemEntity user = new UserSystemEntity("user1", "pass1");
        userSystemDAO.addUser(user);
        Assert.assertEquals(true, userSystemDAO.removeUser(user));
    }

    /**
     * Test the user obtained from the database.
     */
    @Test
    @Transactional
    @Rollback(true)
    public void testGetUser() {
        UserSystemEntity user = new UserSystemEntity("user2", "pass2");
        userSystemDAO.addUser(user);
        Assert.assertEquals(user, userSystemDAO.getUserByUserName("user2"));
    }
}