/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addison.persistence.test;

import com.addison.persistence.entity.UserSystemEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * UserSystemEntityTest class.
 * @author Oscar del Valle Narvaez
 */
public class UserSystemEntityTest {

    /**
     * UserSystem entity object to be tested.
     */
    private UserSystemEntity userSystem;

    /**
     * UserSystemEntityTest constructor.
     */
    public UserSystemEntityTest() {
        // Default empty constructor.
    }
    
    @Before
    public void setUp() {
        userSystem = new UserSystemEntity("user3", "pass3");
    }

    /**
     * Check the user name in setter method.
     */
    @Test
    public void assertUserName() {
        userSystem.setUserName("name1");
        Assert.assertEquals("name1", userSystem.getUserName());
    }

    /**
     * Check the user password in setter method.
     */
    @Test
    public void assertUserPassword() {
        userSystem.setPassword("password1");
        Assert.assertEquals("password1", userSystem.getPassword());
    }
}
