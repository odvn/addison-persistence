package com.addison.persistence.entity;
 
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * UserSystem entity class.
 * @author Oscar del Valle Narváez
 */
@Entity(name="UserSystem")
@Table (name="usersystem")
public class UserSystemEntity implements Serializable {

    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * User name column.
     */
    @Id
    @Column(name = "userName")
    private String userName;

    /**
     * Password column.
     */
    @Column(name = "password")
    private String password;

    /**
     * UserSystemEntity constructor.
     */
    public UserSystemEntity() {
        // Default empty constructor.
    }

    /**
     * UserSystemEntity constructor.
     * @param name User name.
     * @param userPassword User password.
     */
    public UserSystemEntity(String name, String userPassword) {
        super();
        userName = name;
        password = userPassword;
    }

    /**
     * Get the user name.
     * @return The user name.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Set the user name.
     * @param name The user name.
     */
    public void setUserName(String name) {
        userName = name;
    }

    /**
     * Set the user password.
     * @param userPassword The user password.
     */
    public void setPassword(String userPassword) {
        password = userPassword;
    }

    /**
     * Get the user password.
     * @return The user password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * String representation of the entity.
     * @return The string representation of the entity. It contains the user
     *         name and its own password.
     */
    @Override
    public String toString() {
        return "User [userName=" + userName + ", password=" + password + "]";
    }

}