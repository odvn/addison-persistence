package com.addison.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.addison.persistence.entity.UserSystemEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
 
@Repository
@Transactional
public class UserSystemDAOImpl implements UserSystemDAO {
    
    @Autowired
    SessionFactory sessionFactory;
 
    @Override
    public UserSystemEntity getUserByUserName(String userName) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(UserSystemEntity.class, userName);
    }
 
    @Transactional
    @Override
    public boolean addUser(UserSystemEntity dept) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(dept);
        return true;
    }
 
    @Override
    public boolean removeUser(UserSystemEntity dept) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(dept);
        return true;
    }

    @Override
    public void setSessionFactory(SessionFactory factory) {
        sessionFactory = factory;
    }
    
    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}