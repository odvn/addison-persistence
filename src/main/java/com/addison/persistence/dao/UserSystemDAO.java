package com.addison.persistence.dao;

import com.addison.persistence.entity.UserSystemEntity;
import org.hibernate.SessionFactory;

/**
 * UserSytem DAO
 * @author Oscar del Valle Narváez
 */
public interface UserSystemDAO
{
    /**
     * Get the user by user name
     * @param userName The user name.
     * @return The user gotten from the user name.
     */
    public UserSystemEntity getUserByUserName(String userName);

    /**
     * Add a new user to the database.
     * @param user User to be added.
     * @return True if the user has been added, false otherwise.
     */
    public boolean addUser(UserSystemEntity user);

    /**
     * Remove a user in the database.
     * @param user The user to be removed.
     * @return True if the user has been removed, false otherwise.
     */
    public boolean removeUser(UserSystemEntity user);

    /**
     * Set the current session factory.
     * @param factory The current session factory.
     */
    public void setSessionFactory(SessionFactory factory);

    /**
     * Get the current session factory.
     * @return The current session factory.
     */
    public SessionFactory getSessionFactory();
}
